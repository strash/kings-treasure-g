extends Control


signal btn_start_pressed


const TWEEN_SPEED: float = 0.6


# BUILTINS - - - - - - - - -


func _ready() -> void:
	$Girl.rect_position.x = -$Girl.rect_size.x
	$BtnStart.rect_position.y = get_viewport_rect().size.y + ($BtnStart as Button).rect_size.y


# METHODS - - - - - - - - -


func show_view(state: bool) -> void:
	var girl: TextureRect = $Girl as TextureRect
	var btn: Button = $BtnStart as Button
	var _t: int
	if state:
		var btn_y: float = (get_viewport_rect().size.y - btn.rect_size.y) / 2.0
		_t = ($Tween as Tween).interpolate_property(girl, "rect_position:x", -girl.rect_size.x, 0.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		_t = ($Tween as Tween).interpolate_property(btn, "rect_position:y", null, btn_y, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	else:
		var btn_y: float = get_viewport_rect().size.y + btn.rect_size.y
		_t = ($Tween as Tween).interpolate_property(girl, "rect_position:x", 0.0, -girl.rect_size.x, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_IN)
		_t = ($Tween as Tween).interpolate_property(btn, "rect_position:y", null, btn_y, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_IN)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()

# SIGNALS - - - - - - - - -

func _on_BtnStart_pressed() -> void:
	show_view(false)
	emit_signal("btn_start_pressed")
