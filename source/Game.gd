extends Control


signal btn_back_pressed


var SCORE: int = 0
const SCORE_STEP: int = 100

const TIME_MAX: int = 300 # в секундах
var TIMER: int = TIME_MAX

const CARD: Resource = preload("res://source/Card.tscn")
const CARDS: Array = [] # массив карт
const OPEN_CARDS: Array = [] # массыв открытых карт
const CARD_VARIATIONS: int = 4 # количество типов
const CARDS_IN_ROW: int = 4 # количество карт в строке
const CARDS_IN_COLUMN: int = 2 # количество карт в колонке
const BOARD_OFFSET_X: float = 0.0 # отступ доски слева
const BOARD_OFFSET_Y: float = 20.0 # отступ доски сверху
const CARD_WIDTH: float = 102.0 # ширина карты
const CARD_HEIGHT: float = 152.0 # высота карты
const CARD_OFFSET_X: float = 20.0 # отступ между картами в строке
const CARD_OFFSET_Y: float = 20.0 # отступ между картами в колонке
const TWEEN_SPEED: float = 0.6 # скорость анимации

var last_flipped_type: int = -1 # тип последней октрытой карты


# BUILTINS - - - - - - - - -


func _ready() -> void:
	randomize()
	($BtnBack as Button).modulate.a = 0.0
	($Time as Label).modulate.a = 0.0
	($Score as Label).modulate.a = 0.0
	($GameOver as ColorRect).modulate.a = 0.0
	($GameOver/Modal as TextureRect).rect_position.y = get_viewport_rect().size.y + 100.0
	set_time()


func _input(event: InputEvent) -> void:
	# тап по карте
	if event is InputEventScreenTouch and event.is_pressed() and not CARDS.empty():
		for i in CARDS:
			var card: MagicCard = i as MagicCard
			var half_size: Vector2 = Vector2(CARD_WIDTH, CARD_HEIGHT) / 2.0
			if not card.flipped:
				if (card.position.x - half_size.x <= event.position.x and card.position.x + half_size.x >= event.position.x) and (card.position.y - half_size.y <= event.position.y and card.position.y + half_size.y >= event.position.y):
					card.call_deferred("open_card")
					OPEN_CARDS.append(card)
					if last_flipped_type != card.CARD_TYPE and last_flipped_type != -1:
						(OPEN_CARDS.pop_back() as MagicCard).call_deferred("close_card")
						(OPEN_CARDS.pop_back() as MagicCard).call_deferred("close_card")
					last_flipped_type = card.CARD_TYPE if last_flipped_type == -1 else -1
					check_for_win()


func _process(_delta: float) -> void:
	($Score as Label).text = "SCORE: %s" % SCORE


# METHODS - - - - - - - - -


# общая вьюха
func show_view(state: bool) -> void:
	var _t: int
	if state:
		_t = ($Tween as Tween).interpolate_property($BtnBack, "modulate:a", 0.0, 1.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_IN)
		_t = ($Tween as Tween).interpolate_property($Time, "modulate:a", 0.0, 1.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_IN)
		_t = ($Tween as Tween).interpolate_property($Score, "modulate:a", 0.0, 1.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_IN)
		yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
		prepare_game()
		($Timer as Timer).start()
	else:
		_t = ($Tween as Tween).interpolate_property($BtnBack, "modulate:a", 1.0, 0.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		_t = ($Tween as Tween).interpolate_property($Time, "modulate:a", 1.0, 0.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		_t = ($Tween as Tween).interpolate_property($Score, "modulate:a", 1.0, 0.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		clean_board()
		($Timer as Timer).stop()
		TIMER = TIME_MAX
		SCORE = 0
		set_time()
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# вьюха модалки проигрыша
func show_gameover(state: bool) -> void:
	var view: Vector2 = get_viewport_rect().size
	var _t: int
	if state:
		_t = ($Tween as Tween).interpolate_property($GameOver, "modulate:a", null, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($GameOver/Modal, "rect_position:y", null, view.y / 2.0 - $GameOver/Modal.rect_size.y / 2.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	else:
		_t = ($Tween as Tween).interpolate_property($GameOver, "modulate:a", null, 0.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($GameOver/Modal, "rect_position:y", null, view.y + 100.0, TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_IN)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка позиции карты
func set_card_position(card: MagicCard, indise: int) -> void:
	var view: Vector2 = get_viewport_rect().size
	# начальная позиция карты
	card.position = Vector2(view.x / 2.0, view.y + CARD_HEIGHT)
	# размер доски
	var board_width: float = CARD_WIDTH * CARDS_IN_ROW + CARD_OFFSET_X * (CARDS_IN_ROW - 1.0)
	var board_height: float = CARD_HEIGHT * 2.0 + CARD_OFFSET_Y
	# строка
	var column: int = floor(indise as float / 4.0) as int
	# столбец
	var row: int = indise if column == 0 else (indise - CARDS_IN_ROW) as int
	# позиция карты
	var x: float = BOARD_OFFSET_X + (view.x - board_width) / 2.0 + CARD_WIDTH * row + CARD_OFFSET_X * row + CARD_WIDTH / 2.0
	var y: float = BOARD_OFFSET_Y + (view.y - board_height) / 2.0 + CARD_HEIGHT * column + CARD_OFFSET_Y * column + CARD_HEIGHT / 2.0
	var _t: int = ($Tween as Tween).interpolate_property(card, "position", null, Vector2(x, y), TWEEN_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT, indise / 10.0)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func set_time() -> void:
	var sec: int = fmod(TIMER, 60.0) as int
	($Time as Label).text = "Time: %s:%s" % [floor(TIMER as float / 60.0), str(sec) if sec > 9 else "0%s" % sec]


# спавн карт
func prepare_game() -> void:
	# счетчик типов карт
	var type_counter: = []
	for i in CARD_VARIATIONS:
		type_counter.append(0)
	# спавн
	for i in CARDS_IN_ROW * CARDS_IN_COLUMN:
		var card_type: int = randi() % CARD_VARIATIONS
		# одного типа максимум 2 карты
		while type_counter[card_type] == 2:
			card_type = randi() % CARD_VARIATIONS
		type_counter[card_type] += 1
		var card: MagicCard = CARD.instance() as MagicCard
		card.set("CARD_TYPE", card_type)
		CARDS.append(card)
		($Board as Node2D).add_child(card)
		set_card_position(card, i)


# проверка на выигрыш
func check_for_win() -> void:
	if OPEN_CARDS.size() == CARDS_IN_ROW * CARDS_IN_COLUMN:
		var card_speed: float = (OPEN_CARDS[0] as MagicCard).FLIP_SPEED
		yield(get_tree().create_timer(card_speed * 2.0), "timeout")
		clean_board()
		var old_score: int = SCORE
		var _t: int = ($Tween as Tween).interpolate_property(self, "SCORE", old_score, old_score + SCORE_STEP, TWEEN_SPEED / 2.0)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(card_speed), "timeout")
		prepare_game()


func game_over() -> void:
	$GameOver/Modal/Score.text = "SCORE: %s" % SCORE
	show_gameover(true)
	yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
	clean_board()


# очистка доски
func clean_board() -> void:
	for i in CARDS:
		(i as MagicCard).call_deferred("delete_card")
	CARDS.clear()
	OPEN_CARDS.clear()
	last_flipped_type = -1


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	show_view(false)
	show_gameover(false)
	emit_signal("btn_back_pressed")


func _on_BtnReplay_pressed() -> void:
	show_gameover(false)
	TIMER = TIME_MAX
	SCORE = 0
	set_time()
	prepare_game()
	yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
	($Timer as Timer).start()


func _on_Timer_timeout() -> void:
	TIMER -= 1
	set_time()
	if TIMER == 0:
		($Timer as Timer).stop()
		game_over()
