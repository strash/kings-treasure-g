extends Node2D


class_name MagicCard


const CARD_TEXTURES: Array = [
	preload("res://source/assets/image/cards/0.png"),
	preload("res://source/assets/image/cards/1.png"),
	preload("res://source/assets/image/cards/2.png"),
	preload("res://source/assets/image/cards/3.png"),
]
const DEFAULT_TEXTURE: Texture = preload("res://source/assets/image/bg_card.png")

var CARD_TYPE: int
var flipped: bool = false
const FLIP_SPEED: float = 0.15


# BUILTINS - - - - - - - - -


func _ready() -> void:
	pass


# METHODS - - - - - - - - -


func open_card() -> void:
	if not flipped:
		flipped = not flipped
		var _t: int = ($Tween as Tween).interpolate_property($Sprite, "scale:x", null, 0.05, FLIP_SPEED, Tween.TRANS_CUBIC, Tween.EASE_IN)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(FLIP_SPEED), "timeout")
		($Sprite as Sprite).texture = CARD_TEXTURES[CARD_TYPE] as Texture
		_t = ($Tween as Tween).interpolate_property($Sprite, "scale:x", null, 1.0, FLIP_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func close_card() -> void:
	if flipped:
		flipped = not flipped
		var _t: int = ($Tween as Tween).interpolate_property($Sprite, "scale:x", null, 0.05, FLIP_SPEED, Tween.TRANS_CUBIC, Tween.EASE_IN)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(FLIP_SPEED), "timeout")
		($Sprite as Sprite).texture = DEFAULT_TEXTURE as Texture
		_t = ($Tween as Tween).interpolate_property($Sprite, "scale:x", null, 1.0, FLIP_SPEED, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func delete_card() -> void:
	var _t: int
	_t = ($Tween as Tween).interpolate_property($Sprite, "modulate:a", 1.0, 0.0, FLIP_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(FLIP_SPEED), "timeout")
	self.queue_free()


# SIGNALS - - - - - - - - -

