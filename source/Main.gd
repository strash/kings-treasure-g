extends Control


# BUILTINS - - - - - - - - -


func _ready() -> void:
	var _e = $StartScreen.connect("btn_start_pressed", self, "_on_StartScreen_btn_start_pressed")
	_e = $Game.connect("btn_back_pressed", self, "_on_Game_btn_back_pressed")
	$StartScreen.show_view(true)


# METHODS - - - - - - - - -


# SIGNALS - - - - - - - - -


func _on_StartScreen_btn_start_pressed() -> void:
	$Game.show_view(true)


func _on_Game_btn_back_pressed() -> void:
	$StartScreen.show_view(true)
